//
//  ViewController.swift
//  myfirstapp
//
//  Created by IZ Admin on 17/12/15.
//  Copyright © 2015 Iz. All rights reserved.
//

import UIKit

class ViewController: UIViewController, WeatherServiceDelegate {
    
    let weatherService = WeatherService()
    
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBAction func getWeather(sender: AnyObject) {
        openAlert()
    }
    
    func openAlert(){
        let alert = UIAlertController(title: "City", message: "Enter a city name", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler { (textField: UITextField) -> Void in
            textField.placeholder = "City Name"
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil)
        
        alert.addAction(cancel)
        
        let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action: UIAlertAction) -> Void in
            let city = alert.textFields?[0]
            self.weatherService.getWeather((city?.text)!)
        }
        
        alert.addAction(ok)
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func setWeather(weather: Weather){
        self.temp.text = "\(weather.temp)º"
        self.cityLabel.text = weather.city
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.weatherService.delegate = self
        self.weatherService.setDefaults()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

