//
//  WeatherService.swift
//  myfirstapp
//
//  Created by IZ Admin on 18/12/15.
//  Copyright © 2015 Iz. All rights reserved.
//
	
import Foundation

protocol WeatherServiceDelegate {
    func setWeather(weather: Weather)
}

class WeatherService {
    
    enum keys {
        static let temp="temp"
        static let city="city"
    }
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    let endpoint = "http://api.openweathermap.org/data/2.5/weather"
    let appid = "2de143494c0b295cca9337e1e96b00e0"
    
    var delegate: WeatherServiceDelegate?
    
    func getWeather(city: String){
        let url = NSURL(string: self.createURL(city))
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url!) { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            let json = JSON(data: data!)
            let cityName = json["name"].string!
            let temp = self.convertToCelsius(json["main"]["temp"].double!)
            let weather = Weather(city: cityName, temp: temp)
            if(self.delegate != nil) {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.delegate?.setWeather(weather)
                })
            }
        }
        task.resume()
    }
    
    func createURL(city: String) -> String{
        return "\(endpoint)?q=\(city)&appid=\(appid)".stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    }
    
    func convertToCelsius(kelvin: Double) -> Int {
        return Int(kelvin - 273.15)
    }
    
    func saveData(weather: Weather){
        defaults.setValue(weather.temp, forKey: keys.temp)
        defaults.setValue(weather.city, forKey: keys.city)
    }
    
    func setDefaults(){
        let temp = defaults.integerForKey(keys.temp)
        let city = defaults.stringForKey(keys.city) ?? ""
        let weather = Weather(city: city, temp: temp)
        self.delegate?.setWeather(weather)
    }
}