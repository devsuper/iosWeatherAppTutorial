//
//  Weather.swift
//  myfirstapp
//
//  Created by IZ Admin on 18/12/15.
//  Copyright © 2015 Iz. All rights reserved.
//

import Foundation


struct Weather {
    
    let city: String
    let temp: Int
    
    init(city: String, temp: Int){
        self.city = city
        self.temp = temp
    }
}